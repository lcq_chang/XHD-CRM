﻿/*
* CRM_Contact.cs
*
* 功 能： N/A
* 类 名： CRM_Contact
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Web;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class MSG_Task
    {
        public static BLL.MSG_Task task = new BLL.MSG_Task();
        public static Model.MSG_Task model = new Model.MSG_Task();

        public HttpContext Context;
        public int emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;


        public MSG_Task()
        {
        }

        public MSG_Task(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.ID;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
        }
        public string form(string id)
        {
            string dt;
            if (PageValidate.IsNumber(id))
            {
                DataSet ds = task.GetList(string.Format("id = {0}", int.Parse(id)));
                dt = DataToJson.DataToJSON(ds);
            }
            else
            {
                dt = "{}";
            }
            return dt;
        }

        public void save()
        {
            model.task_title = PageValidate.InputText(request["T_task_name"], 255);

            if (PageValidate.IsNumber(request["T_task_type_val"]))
                model.task_type_id = int.Parse(request["T_task_type_val"]);

            if (PageValidate.IsNumber(request["T_customer_val"]))
                model.customer_id = int.Parse(request["T_customer_val"]);

            if (PageValidate.IsNumber(request["T_executive_val"]))
                model.executive_id = int.Parse(request["T_executive_val"]);

            if (PageValidate.IsNumber(request["T_status_val"]))
                model.task_status_id = int.Parse(request["T_status_val"]);

            if (PageValidate.IsNumber(request["T_priority_val"]))
                model.Priority_id = int.Parse(request["T_priority_val"]);

            model.executive_time = DateTime.Parse(request["T_executive_time"]);
            model.task_content = PageValidate.InputText(request["T_content"], int.MaxValue);


            string id = PageValidate.InputText(request["id"], 50);
            if (PageValidate.IsNumber(id))
            {
                DataSet ds = task.GetList("id=" + int.Parse(id));
                DataRow dr = ds.Tables[0].Rows[0];

                model.id = int.Parse(id);
                model.update_id = emp_id;
                model.update_time = DateTime.Now;

                task.Update(model);

                //日志
                var log = new sys_log();

                int UserID = emp_id;
                string UserName = emp_name;
                string IPStreet = request.UserHostAddress;
                string EventTitle = model.task_title;
                ;
                string EventType = "任务修改";
                int EventID = model.id;
                string Log_Content = null;

                if (dr["task_title"].ToString() != request["T_task_name"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "任务名称", dr["task_title"], request["T_task_name"]);

                if (dr["task_type"].ToString() != request["T_task_type"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "任务类别", dr["task_type"], request["T_task_type"]);

                if (dr["customer"].ToString() != request["T_customer"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "关联客户", dr["customer"], request["T_customer"]);

                if (dr["executive"].ToString() != request["T_executive"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "执行人", dr["executive"], request["T_executive"]);

                if (dr["task_status_id"].ToString() != request["T_status_val"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "任务状态", dr["task_status_id"], request["T_status_val"]);

                if (dr["Priority_id"].ToString() != request["T_priority_val"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "优先级", dr["Priority_id"], request["T_priority_val"]);

                if (dr["executive_time"].ToString() != DateTime.Parse( request["T_executive_time"]).ToString())
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "执行时间", dr["executive_time"], request["T_executive_time"]);

                if (dr["task_content"].ToString() != request["T_content"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "任务描述", "任务内容改变", null);

                if (!string.IsNullOrEmpty(Log_Content))
                    log.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, Log_Content);
            }
            else
            {
                model.assign_id = emp_id;
                model.create_id = emp_id;
                model.create_time = DateTime.Now;

                task.Add(model);
            }
        }

        public void stopTask(int id)
        {
            UpdateStatu(id, 2);
        }

        public void UpdateStatu(int id, int status)
        {
            model.id = id;
            model.task_status_id = status;

            task.UpdateStatu(model);
        }

        public string grid()
        {
            int PageIndex = int.Parse(request["page"] == null ? "1" : request["page"]);
            int PageSize = int.Parse(request["pagesize"] == null ? "30" : request["pagesize"]);
            string sortname = request["sortname"];
            string sortorder = request["sortorder"];

            if (string.IsNullOrEmpty(sortname))
                sortname = " id ";
            if (string.IsNullOrEmpty(sortorder))
                sortorder = " desc";

            string sorttext = " " + sortname + " " + sortorder;

            string Total;
            string serchtxt = "1=1";

            if (PageValidate.IsNumber(request["executive_employee_val"]))
                serchtxt += string.Format(" and executive_id = {0}", int.Parse(request["executive_employee_val"]));

            if (PageValidate.IsNumber(request["assign_employee_val"]))
                serchtxt += string.Format(" and assign_id = {0}", int.Parse(request["assign_employee_val"]));

            if (PageValidate.IsNumber(request["task_status_val"]))
                serchtxt += string.Format(" and task_status_id = {0}", int.Parse(request["task_status_val"]));

            if (!string.IsNullOrEmpty(request["taskname"]))
                serchtxt += string.Format(" and task_title like N'%{0}%'", PageValidate.InputText(request["taskname"], 255));            

            if (PageValidate.IsDateTime(request["startdate"]))
                serchtxt += string.Format(" and executive_time >= '{0}' ", PageValidate.InputText(request["startdate"], 255));

            if (PageValidate.IsDateTime(request["enddate"]))
            {
                DateTime enddate = DateTime.Parse(request["enddate"]).AddHours(23).AddMinutes(59).AddSeconds(59);
                serchtxt += string.Format(" and executive_time <= '{0}' ", enddate);
            }

            if (PageValidate.IsDateTime(request["startcreate"]))
                serchtxt += string.Format(" and create_time >= '{0}' ", PageValidate.InputText(request["startcreate"], 255));

            if (PageValidate.IsDateTime(request["endcreate"]))
            {
                DateTime enddate = DateTime.Parse(request["endcreate"]).AddHours(23).AddMinutes(59).AddSeconds(59);
                serchtxt += string.Format(" and create_time <= '{0}' ", enddate);
            }
            //权限     
            string requesttype=request["type"];
            if (requesttype == "assign")
                serchtxt += string.Format(" and assign_id={0}", emp_id);
            else if (requesttype == "executive")
                serchtxt += string.Format(" and executive_id={0}", emp_id);

            //return (serchtxt);

            DataSet ds = task.GetList(PageSize, PageIndex, serchtxt, sorttext, out Total);

            string dt = GetGridJSON.DataTableToJSON1(ds.Tables[0], Total);

            return dt;

        }
       
        //del
        public string del(int id)
        {
            bool canDel = false;

            if (uid == "admin")
            {
                canDel = true;
            }
            else
            {
                var getauth = new GetAuthorityByUid();
                canDel = getauth.GetBtnAuthority(emp_id.ToString(), "112");
                if (!canDel)
                    return ("auth");
            }

            if (canDel)
            {
                DataSet ds = task.GetList("id=" + id);

                string EventType = "删除任务";

                bool isdel = task.Delete(id);
                if (isdel)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        int UserID = emp_id;
                        string UserName = emp_name;
                        string IPStreet = request.UserHostAddress;
                        int EventID = id;
                        string EventTitle = ds.Tables[0].Rows[i]["task_title"].ToString();

                        var log = new sys_log();
                        log.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, null);
                    }
                    return ("true");
                    BLL.MSG_Task_follow follow = new BLL.MSG_Task_follow();
                    follow.DeleteWhere("task_id=" + id);
                }
                return ("false");
            }
            return ("auth");
        }

        public string TaskRemind()
        {
            DataSet ds = task.GetList(7, "executive_id=" + emp_id, "executive_time desc");
            string dt = GetGridJSON.DataTableToJSON(ds.Tables[0]);
            return dt;
        }
    }
}
