/*
* Sys_authority.cs
*
* 功 能： N/A
* 类 名： Sys_authority
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;

namespace XHD.Model
{
    /// <summary>
    ///     Sys_authority:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class Sys_authority
    {
        #region Model

        private string _app_ids;
        private string _button_ids;
        private DateTime? _create_date;
        private int? _create_id;
        private string _menu_ids;
        private int _role_id;

        /// <summary>
        /// </summary>
        public int Role_id
        {
            set { _role_id = value; }
            get { return _role_id; }
        }

        /// <summary>
        /// </summary>
        public string App_ids
        {
            set { _app_ids = value; }
            get { return _app_ids; }
        }

        /// <summary>
        /// </summary>
        public string Menu_ids
        {
            set { _menu_ids = value; }
            get { return _menu_ids; }
        }

        /// <summary>
        /// </summary>
        public string Button_ids
        {
            set { _button_ids = value; }
            get { return _button_ids; }
        }

        /// <summary>
        /// </summary>
        public int? Create_id
        {
            set { _create_id = value; }
            get { return _create_id; }
        }

        /// <summary>
        /// </summary>
        public DateTime? Create_date
        {
            set { _create_date = value; }
            get { return _create_date; }
        }

        #endregion Model
    }
}