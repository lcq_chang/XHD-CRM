﻿/*
* Sys_mail_list.cs
*
* 功 能： N/A
* 类 名： Sys_mail_list
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-07-20 09:50:36    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com  www.xhdoa.com. All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：小黄豆                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using XHD.DBUtility;

//Please add references

namespace XHD.DAL
{
    /// <summary>
    ///     数据访问类:Sys_mail_list
    /// </summary>
    public class Sys_mail_list
    {
        #region  BasicMethod

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public int Add(Model.Sys_mail_list model)
        {
            var strSql = new StringBuilder();
            strSql.Append("insert into Sys_mail_list(");
            strSql.Append("mail_type_id,email,password,belong_id,mail_enable)");
            strSql.Append(" values (");
            strSql.Append("@mail_type_id,@email,@password,@belong_id,@mail_enable)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters =
            {
                new SqlParameter("@mail_type_id", SqlDbType.Int, 4),
                new SqlParameter("@email", SqlDbType.VarChar, 250),
                new SqlParameter("@password", SqlDbType.VarChar, 250),
                new SqlParameter("@belong_id", SqlDbType.Int, 4),
                new SqlParameter("@mail_enable", SqlDbType.Int, 4)
            };
            parameters[0].Value = model.mail_type_id;
            parameters[1].Value = model.email;
            parameters[2].Value = model.password;
            parameters[3].Value = model.belong_id;
            parameters[4].Value = model.mail_enable;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            return Convert.ToInt32(obj);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.Sys_mail_list model)
        {
            var strSql = new StringBuilder();
            strSql.Append("update Sys_mail_list set ");
            strSql.Append("mail_type_id=@mail_type_id,");
            strSql.Append("email=@email,");
            strSql.Append("password=@password,");
            strSql.Append("belong_id=@belong_id,");
            strSql.Append("mail_enable=@mail_enable");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@mail_type_id", SqlDbType.Int, 4),
                new SqlParameter("@email", SqlDbType.VarChar, 250),
                new SqlParameter("@password", SqlDbType.VarChar, 250),
                new SqlParameter("@belong_id", SqlDbType.Int, 4),
                new SqlParameter("@mail_enable", SqlDbType.Int, 4),
                new SqlParameter("@id", SqlDbType.Int, 4)
            };
            parameters[0].Value = model.mail_type_id;
            parameters[1].Value = model.email;
            parameters[2].Value = model.password;
            parameters[3].Value = model.belong_id;
            parameters[4].Value = model.mail_enable;
            parameters[5].Value = model.id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(int id)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from Sys_mail_list ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@id", SqlDbType.Int, 4)
            };
            parameters[0].Value = id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     批量删除数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from Sys_mail_list ");
            strSql.Append(" where id in (" + idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            var strSql = new StringBuilder();
            strSql.Append("select id,mail_type_id,email,password,belong_id,mail_enable ");
            strSql.Append(",(select mail_type from Sys_mail_type where id = Sys_mail_list.mail_type_id) as mail_type ");
            strSql.Append(",(select name from hr_employee where id = Sys_mail_list.belong_id) as belong ");
            strSql.Append(" FROM Sys_mail_list ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            var strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top);
            }
            strSql.Append(" id,mail_type_id,email,password,belong_id,mail_enable ");
            strSql.Append(",(select mail_type from Sys_mail_type where id = Sys_mail_list.mail_type_id) as mail_type ");
            strSql.Append(",(select name from hr_employee where id = Sys_mail_list.belong_id) as belong ");
            strSql.Append(" FROM Sys_mail_list ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            var strSql_grid = new StringBuilder();
            var strSql_total = new StringBuilder();
            strSql_total.Append(" SELECT COUNT(id) FROM Sys_mail_list ");
            strSql_grid.Append("SELECT ");
            strSql_grid.Append("      n,id,mail_type_id,email,password,belong_id,mail_enable ");
            strSql_grid.Append(",(select mail_type from Sys_mail_type where id = w1.mail_type_id) as mail_type ");
            strSql_grid.Append(",(select name from hr_employee where id = w1.belong_id) as belong ");
            strSql_grid.Append( " FROM ( SELECT id,mail_type_id,email,password,belong_id,mail_enable, ROW_NUMBER() OVER( Order by " + filedOrder + " ) AS n from Sys_mail_list");
            if (strWhere.Trim() != "")
            {
                strSql_grid.Append(" WHERE " + strWhere);
                strSql_total.Append(" WHERE " + strWhere);
            }
            strSql_grid.Append("  ) as w1  ");
            strSql_grid.Append("WHERE n BETWEEN " + PageSize*(PageIndex - 1) + " AND " + PageSize*PageIndex);
            strSql_grid.Append(" ORDER BY " + filedOrder);
            Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
            return DbHelperSQL.Query(strSql_grid.ToString());
        }

        #endregion  BasicMethod

        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}