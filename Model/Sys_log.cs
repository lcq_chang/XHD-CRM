﻿/*
* Sys_log.cs
*
* 功 能： N/A
* 类 名： Sys_log
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-24 18:54:40    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;

namespace XHD.Model
{
    /// <summary>
    ///     Sys_log:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class Sys_log
    {
        #region Model

        private DateTime? _eventdate;
        private string _eventid;
        private string _eventtitle;
        private string _eventtype;
        private int _id;
        private string _ipstreet;
        private string _log_content;
        private int? _userid;

        /// <summary>
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }

        /// <summary>
        /// </summary>
        public string EventType
        {
            set { _eventtype = value; }
            get { return _eventtype; }
        }

        /// <summary>
        /// </summary>
        public string EventID
        {
            set { _eventid = value; }
            get { return _eventid; }
        }

        /// <summary>
        /// </summary>
        public string EventTitle
        {
            set { _eventtitle = value; }
            get { return _eventtitle; }
        }

        /// <summary>
        /// </summary>
        public int? UserID
        {
            set { _userid = value; }
            get { return _userid; }
        }

        /// <summary>
        /// </summary>
        public string IPStreet
        {
            set { _ipstreet = value; }
            get { return _ipstreet; }
        }

        /// <summary>
        /// </summary>
        public DateTime? EventDate
        {
            set { _eventdate = value; }
            get { return _eventdate; }
        }

        /// <summary>
        /// </summary>
        public string Log_Content
        {
            set { _log_content = value; }
            get { return _log_content; }
        }

        #endregion Model
    }
}