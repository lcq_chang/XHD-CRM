﻿/*
* CRM_Customer.cs
*
* 功 能： N/A
* 类 名： CRM_Customer
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-22 11:32:23    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using XHD.DBUtility;

//Please add references

namespace XHD.DAL
{
    /// <summary>
    ///     数据访问类:CRM_Customer
    /// </summary>
    public class CRM_Customer
    {
        #region  BasicMethod

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public int Add(Model.CRM_Customer model)
        {
            var strSql = new StringBuilder();
            strSql.Append("insert into CRM_Customer(");
            strSql.Append(
                "Serialnumber,Customer,address,tel,fax,site,industry_id,Provinces_id,City_id,CustomerType_id,CustomerLevel_id,CustomerSource_id,DesCripe,Remarks,Department_id,Employee_id,privatecustomer,lastfollow,Create_id,Create_name,Create_date,isDelete,Delete_time,xy)");
            strSql.Append(" values (");
            strSql.Append(
                "@Serialnumber,@Customer,@address,@tel,@fax,@site,@industry_id,@Provinces_id,@City_id,@CustomerType_id,@CustomerLevel_id,@CustomerSource_id,@DesCripe,@Remarks,@Department_id,@Employee_id,@privatecustomer,@lastfollow,@Create_id,@Create_name,@Create_date,@isDelete,@Delete_time,@xy)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters =
            {
                new SqlParameter("@Serialnumber", SqlDbType.VarChar, 250),
                new SqlParameter("@Customer", SqlDbType.VarChar, 250),
                new SqlParameter("@address", SqlDbType.VarChar, 250),
                new SqlParameter("@tel", SqlDbType.VarChar, 250),
                new SqlParameter("@fax", SqlDbType.VarChar, 250),
                new SqlParameter("@site", SqlDbType.VarChar, 250),
                new SqlParameter("@industry_id", SqlDbType.Int, 4),
                new SqlParameter("@Provinces_id", SqlDbType.Int, 4),
                new SqlParameter("@City_id", SqlDbType.Int, 4),
                new SqlParameter("@CustomerType_id", SqlDbType.Int, 4),
                new SqlParameter("@CustomerLevel_id", SqlDbType.Int, 4),
                new SqlParameter("@CustomerSource_id", SqlDbType.Int, 4),
                new SqlParameter("@DesCripe", SqlDbType.VarChar, 4000),
                new SqlParameter("@Remarks", SqlDbType.VarChar, 4000),
                new SqlParameter("@Department_id", SqlDbType.Int, 4),
                new SqlParameter("@Employee_id", SqlDbType.Int, 4),
                new SqlParameter("@privatecustomer", SqlDbType.VarChar, 50),
                new SqlParameter("@lastfollow", SqlDbType.DateTime),
                new SqlParameter("@Create_id", SqlDbType.Int, 4),
                new SqlParameter("@Create_name", SqlDbType.VarChar, 250),
                new SqlParameter("@Create_date", SqlDbType.DateTime),
                new SqlParameter("@isDelete", SqlDbType.Int, 4),
                new SqlParameter("@Delete_time", SqlDbType.DateTime),
                new SqlParameter("@xy", SqlDbType.VarChar,50)
            };
            parameters[0].Value = model.Serialnumber;
            parameters[1].Value = model.Customer;
            parameters[2].Value = model.address;
            parameters[3].Value = model.tel;
            parameters[4].Value = model.fax;
            parameters[5].Value = model.site;
            parameters[6].Value = model.industry_id;
            parameters[7].Value = model.Provinces_id;
            parameters[8].Value = model.City_id;
            parameters[9].Value = model.CustomerType_id;
            parameters[10].Value = model.CustomerLevel_id;
            parameters[11].Value = model.CustomerSource_id;
            parameters[12].Value = model.DesCripe;
            parameters[13].Value = model.Remarks;
            parameters[14].Value = model.Department_id;
            parameters[15].Value = model.Employee_id;
            parameters[16].Value = model.privatecustomer;
            parameters[17].Value = model.lastfollow;
            parameters[18].Value = model.Create_id;
            parameters[19].Value = model.Create_name;
            parameters[20].Value = model.Create_date;
            parameters[21].Value = model.isDelete;
            parameters[22].Value = model.Delete_time;
            parameters[23].Value = model.xy;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            return Convert.ToInt32(obj);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.CRM_Customer model)
        {
            var strSql = new StringBuilder();
            strSql.Append("update CRM_Customer set ");
            strSql.Append("Serialnumber=@Serialnumber,");
            strSql.Append("Customer=@Customer,");
            strSql.Append("address=@address,");
            strSql.Append("tel=@tel,");
            strSql.Append("fax=@fax,");
            strSql.Append("site=@site,");
            strSql.Append("industry_id=@industry_id,");
            strSql.Append("Provinces_id=@Provinces_id,");
            strSql.Append("City_id=@City_id,");
            strSql.Append("CustomerType_id=@CustomerType_id,");
            strSql.Append("CustomerLevel_id=@CustomerLevel_id,");
            strSql.Append("CustomerSource_id=@CustomerSource_id,");
            strSql.Append("DesCripe=@DesCripe,");
            strSql.Append("Remarks=@Remarks,");
            strSql.Append("Department_id=@Department_id,");
            strSql.Append("Employee_id=@Employee_id,");
            strSql.Append("privatecustomer=@privatecustomer,");
            strSql.Append("xy=@xy");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@Serialnumber", SqlDbType.VarChar, 250),
                new SqlParameter("@Customer", SqlDbType.VarChar, 250),
                new SqlParameter("@address", SqlDbType.VarChar, 250),
                new SqlParameter("@tel", SqlDbType.VarChar, 250),
                new SqlParameter("@fax", SqlDbType.VarChar, 250),
                new SqlParameter("@site", SqlDbType.VarChar, 250),
                new SqlParameter("@industry_id", SqlDbType.Int, 4),
                new SqlParameter("@Provinces_id", SqlDbType.Int, 4),
                new SqlParameter("@City_id", SqlDbType.Int, 4),
                new SqlParameter("@CustomerType_id", SqlDbType.Int, 4),
                new SqlParameter("@CustomerLevel_id", SqlDbType.Int, 4),
                new SqlParameter("@CustomerSource_id", SqlDbType.Int, 4),
                new SqlParameter("@DesCripe", SqlDbType.VarChar, 4000),
                new SqlParameter("@Remarks", SqlDbType.VarChar, 4000),
                new SqlParameter("@Department_id", SqlDbType.Int, 4),
                new SqlParameter("@Employee_id", SqlDbType.Int, 4),
                new SqlParameter("@privatecustomer", SqlDbType.VarChar, 50),
                new SqlParameter("@xy", SqlDbType.VarChar,50),
                new SqlParameter("@id", SqlDbType.Int, 4)
            };
            parameters[0].Value = model.Serialnumber;
            parameters[1].Value = model.Customer;
            parameters[2].Value = model.address;
            parameters[3].Value = model.tel;
            parameters[4].Value = model.fax;
            parameters[5].Value = model.site;
            parameters[6].Value = model.industry_id;
            parameters[7].Value = model.Provinces_id;
            parameters[8].Value = model.City_id;
            parameters[9].Value = model.CustomerType_id;
            parameters[10].Value = model.CustomerLevel_id;
            parameters[11].Value = model.CustomerSource_id;
            parameters[12].Value = model.DesCripe;
            parameters[13].Value = model.Remarks;
            parameters[14].Value = model.Department_id;
            parameters[15].Value = model.Employee_id;
            parameters[16].Value = model.privatecustomer;
            parameters[17].Value = model.xy;
            parameters[18].Value = model.id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(int id)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from CRM_Customer ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@id", SqlDbType.Int, 4)
            };
            parameters[0].Value = id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     批量删除数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from CRM_Customer ");
            strSql.Append(" where id in (" + idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        public DataSet GetMapList(string strWhere)
        {
            var strSql = new StringBuilder();
            strSql.Append("SELECT ");
            strSql.Append("     [id],[Customer],[address],[tel],[xy],[Department_id],[Employee_id]      ");
            strSql.Append("FROM [dbo].[CRM_Customer] ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            var strSql = new StringBuilder();
            strSql.Append("SELECT ");
            strSql.Append("     [id],[Serialnumber],[Customer],[address],[tel],[fax],[site],[industry_id],[Provinces_id] ");
            strSql.Append("     ,[City_id],[CustomerType_id] ,[CustomerLevel_id],[CustomerSource_id] ,[DesCripe],[Remarks],xy ");
            strSql.Append("     ,(select params_name from Param_SysParam where id=CRM_Customer.[industry_id]) as [industry]   ");
            strSql.Append("     ,(select City from Param_City where id=CRM_Customer.[Provinces_id]) as [Provinces]       ");
            strSql.Append("     ,(select City from Param_City where id=CRM_Customer.[City_id]) as [City]       ");
            strSql.Append("     ,(select params_name from Param_SysParam where id=CRM_Customer.[CustomerType_id]) as[CustomerType]      ");
            strSql.Append("     ,(select params_name from Param_SysParam where id=CRM_Customer.[CustomerLevel_id]) as[CustomerLevel]       ");
            strSql.Append("     ,(select params_name from Param_SysParam where id=CRM_Customer.[CustomerSource_id]) as[CustomerSource]   ");
            strSql.Append("     ,(select d_name from hr_department where id=CRM_Customer.[Department_id]) as [Department] ");
            strSql.Append("     ,(select name from hr_employee where ID=CRM_Customer.Employee_id) as[Employee] ");
            strSql.Append("     ,[Department_id],[Employee_id],[privatecustomer] ,[lastfollow]      ");
            strSql.Append("     ,[Create_id],[Create_name],[Create_date],[isDelete],[Delete_time] ");
            strSql.Append("FROM [dbo].[CRM_Customer] ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet ToExcel(int Top, string strWhere, string filedOrder)
        {
            var strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top);
            }
            strSql.Append("     [Customer],[address],[tel],[fax],[site] ");
            strSql.Append("     ,(select params_name from Param_SysParam where id=CRM_Customer.[industry_id]) as [industry]   ");
            strSql.Append("     ,(select City from Param_City where id=CRM_Customer.[Provinces_id]) as [Provinces]       ");
            strSql.Append("     ,(select City from Param_City where id=CRM_Customer.[City_id]) as [City]       ");
            strSql.Append("     ,(select params_name from Param_SysParam where id=CRM_Customer.[CustomerType_id]) as[CustomerType]      ");
            strSql.Append("     ,(select params_name from Param_SysParam where id=CRM_Customer.[CustomerLevel_id]) as[CustomerLevel]       ");
            strSql.Append("     ,(select params_name from Param_SysParam where id=CRM_Customer.[CustomerSource_id]) as[CustomerSource]   ");
            strSql.Append("     ,(select d_name from hr_department where id=CRM_Customer.[Department_id]) as [Department] ");
            strSql.Append("     ,(select name from hr_employee where ID=CRM_Customer.Employee_id) as[Employee] ");
            strSql.Append("     ,[DesCripe],[Remarks],[privatecustomer] ");
            strSql.Append("FROM [dbo].[CRM_Customer] ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }
        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            var strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top);
            }
            strSql.Append("     [id],[Serialnumber],[Customer],[address],[tel],[fax],[site],[industry_id],[Provinces_id] ");
            strSql.Append("     ,[City_id],[CustomerType_id] ,[CustomerLevel_id],[CustomerSource_id] ,[DesCripe],[Remarks],xy ");
            strSql.Append("     ,(select params_name from Param_SysParam where id=CRM_Customer.[industry_id]) as [industry]   ");
            strSql.Append("     ,(select City from Param_City where id=CRM_Customer.[Provinces_id]) as [Provinces]       ");
            strSql.Append("     ,(select City from Param_City where id=CRM_Customer.[City_id]) as [City]       ");
            strSql.Append("     ,(select params_name from Param_SysParam where id=CRM_Customer.[CustomerType_id]) as[CustomerType]      ");
            strSql.Append("     ,(select params_name from Param_SysParam where id=CRM_Customer.[CustomerLevel_id]) as[CustomerLevel]       ");
            strSql.Append("     ,(select params_name from Param_SysParam where id=CRM_Customer.[CustomerSource_id]) as[CustomerSource]   ");
            strSql.Append("     ,(select d_name from hr_department where id=CRM_Customer.[Department_id]) as [Department] ");
            strSql.Append("     ,(select name from hr_employee where ID=CRM_Customer.Employee_id) as[Employee] ");
            strSql.Append("     ,[Department_id],[Employee_id],[privatecustomer] ,[lastfollow]      ");
            strSql.Append("     ,[Create_id],[Create_name],[Create_date],[isDelete],[Delete_time] ");
            strSql.Append("FROM [dbo].[CRM_Customer] ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            var strSql_grid = new StringBuilder();
            var strSql_total = new StringBuilder();
            strSql_total.Append(" select count(id) FROM CRM_Customer ");
            strSql_grid.Append(" SELECT ");
            strSql_grid.Append("     n,[id],[Serialnumber],[Customer],[address],[tel],[fax],[site],[industry_id],[Provinces_id] ");
            strSql_grid.Append("     ,[City_id],[CustomerType_id] ,[CustomerLevel_id],[CustomerSource_id] ,[DesCripe],[Remarks],xy ");
            strSql_grid.Append("     ,(select params_name from Param_SysParam where id=w1.[industry_id]) as [industry]   ");
            strSql_grid.Append("     ,(select City from Param_City where id=w1.[Provinces_id]) as [Provinces]       ");
            strSql_grid.Append("     ,(select City from Param_City where id=w1.[City_id]) as [City]       ");
            strSql_grid.Append("     ,(select params_name from Param_SysParam where id=w1.[CustomerType_id]) as[CustomerType]      ");
            strSql_grid.Append("     ,(select params_name from Param_SysParam where id=w1.[CustomerLevel_id]) as[CustomerLevel]       ");
            strSql_grid.Append("     ,(select params_name from Param_SysParam where id=w1.[CustomerSource_id]) as[CustomerSource]   ");
            strSql_grid.Append("     ,(select d_name from hr_department where id=w1.[Department_id]) as [Department] ");
            strSql_grid.Append("     ,(select name from hr_employee where ID=w1.Employee_id) as[Employee] ");
            strSql_grid.Append("     ,[Department_id],[Employee_id],[privatecustomer] ,[lastfollow]      ");
            strSql_grid.Append("     ,[Create_id],[Create_name],[Create_date],[isDelete],[Delete_time] ");
            strSql_grid.Append(" FROM ( SELECT *, ROW_NUMBER() OVER( Order by " + filedOrder +" ) AS n from [dbo].[CRM_Customer]");
            if (strWhere.Trim() != "")
            {
                strSql_grid.Append(" where " + strWhere);
                strSql_total.Append(" where " + strWhere);
            }
            strSql_grid.Append("  ) as w1  ");
            strSql_grid.Append("WHERE n BETWEEN " + PageSize * (PageIndex - 1) + " AND " + PageSize * PageIndex);
            strSql_grid.Append(" ORDER BY " + filedOrder + "   ");
            Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
            return DbHelperSQL.Query(strSql_grid.ToString());
        }

        #endregion  BasicMethod

        #region  ExtensionMethod

        /// <summary>
        ///     批量转客户
        /// </summary>
        public bool Update_batch(Model.CRM_Customer model, string strWhere)
        {
            var strSql = new StringBuilder();
            strSql.Append("update CRM_Customer set ");
            strSql.Append("Department_id=@Department_id,");
            strSql.Append("Employee_id=@Employee_id,");
            strSql.Append(" where Employee_id=@Create_id");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" and " + strWhere);
            }
            SqlParameter[] parameters =
            {
                new SqlParameter("@Department_id", SqlDbType.Int, 4),
                new SqlParameter("@Employee_id", SqlDbType.Int, 4),
                new SqlParameter("@Create_id", SqlDbType.Int, 4)
            };
            parameters[0].Value = model.Department_id;
            parameters[1].Value = model.Employee_id;
            parameters[2].Value = model.Create_id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     预删除
        /// </summary>
        public bool AdvanceDelete(int id, int isDelete, string time)
        {
            var strSql = new StringBuilder();
            strSql.Append("update CRM_Customer set ");
            strSql.Append("isDelete=" + isDelete);
            strSql.Append(",Delete_time='" + time + "'");
            strSql.Append(" where id=" + id);
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     更新最后跟进
        /// </summary>
        public bool UpdateLastFollow(string id)
        {
            var strSql = new StringBuilder();
            strSql.Append("update CRM_Customer set ");
            strSql.Append(
                "[lastfollow] = isnull((select max(Follow_date) as Followdate from dbo.CRM_Follow where CRM_Customer.id=CRM_Follow.Customer_id),Create_date)");
            strSql.Append(" where CRM_Customer.id=" + id);

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        public DataSet Reports_year(string items, int year, string where)
        {
            var strSql = new StringBuilder();
            strSql.Append("if OBJECT_ID('Tempdb..#t') is not null ");
            strSql.Append("    drop TABLE  #t ");
            //strSql.Append("go");
            strSql.Append(" begin ");
            //strSql.Append("    --预统计表 #t");
            strSql.Append("    select ");
            strSql.Append("        params_name,'m'+convert(varchar,month(Create_date)) mm,count(id)tNum into #t ");
            strSql.Append("   from (select w1.*,(select params_name from Param_SysParam where id = w1." + items + "_id) as params_name from CRM_Customer w1 ");
            strSql.Append("    where datediff(YEAR,w1.[Create_date],'" + year + "-1-1')=0 and isDelete=0 ) w2 ");
            if (where.Trim() != "")
            {
                strSql.Append(" and " + where);
            }
            strSql.Append("    group by w2.params_name,'m'+convert(varchar,month(w2.Create_date)) ");

            //strSql.Append("    --生成SQL");
            strSql.Append("    declare @sql varchar(8000) ");
            strSql.Append("    set @sql='select params_name items ' ");
            strSql.Append("    select @sql = @sql + ',sum(case mm when ' + char(39) +mm+ char(39) + ' then tNum else 0 end) ['+ mm +']' ");
            strSql.Append("        from (select distinct mm from #t)as data ");
            strSql.Append("    set @sql = @sql + ' from #t group by params_name' ");

            strSql.Append("    exec(@sql) ");
            strSql.Append(" end ");

            return DbHelperSQL.Query(strSql.ToString());
        }


        /// <summary>
        ///     同比环比【客户新增】
        /// </summary>
        /// <param name="dt1"></param>
        /// <param name="dt2"></param>
        /// <returns></returns>
        public DataSet Compared(string year1, string month1, string year2, string month2)
        {
            var strSql = new StringBuilder();
            strSql.Append(" select count(id) as yy,");
            strSql.Append(" SUM(case when YEAR( Create_date)=('" + year1 + "') and MONTH(Create_date)=('" + month1 + "') then 1 else 0 end) as dt1, ");
            strSql.Append(" SUM(case when YEAR( Create_date)=('" + year2 + "') and MONTH(Create_date)=('" + month2 + "') then 1 else 0 end) as dt2 ");
            strSql.Append(" FROM CRM_Customer WHERE isDelete=0 ");

            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     客户类型【同比环比】
        /// </summary>
        /// <param name="dt1"></param>
        /// <param name="dt2"></param>
        /// <returns></returns>
        public DataSet Compared_type(string year1, string month1, string year2, string month2)
        {
            var strSql = new StringBuilder();
            strSql.Append(" select CustomerType as yy,count(CustomerType) as xx,");
            strSql.Append(" SUM(case when YEAR( Create_date)=('" + year1 + "') and MONTH(Create_date)=('" + month1 + "') then 1 else 0 end) as dt1, ");
            strSql.Append(" SUM(case when YEAR( Create_date)=('" + year2 + "') and MONTH(Create_date)=('" + month2 + "') then 1 else 0 end) as dt2 ");
            strSql.Append(" FROM (select (select params_name from Param_SysParam where id=CRM_Customer.[CustomerType_id]) as CustomerType,* from CRM_Customer) as w1  WHERE isDelete=0  group by CustomerType");

            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     客户级别【同比环比】
        /// </summary>
        /// <param name="dt1"></param>
        /// <param name="dt2"></param>
        /// <returns></returns>
        public DataSet Compared_level(string year1, string month1, string year2, string month2)
        {
            var strSql = new StringBuilder();
            strSql.Append(" select CustomerLevel as yy,count(CustomerLevel) as xx,");
            strSql.Append(" SUM(case when YEAR( Create_date)=('" + year1 + "') and MONTH(Create_date)=('" + month1 + "') then 1 else 0 end) as dt1, ");
            strSql.Append(" SUM(case when YEAR( Create_date)=('" + year2 + "') and MONTH(Create_date)=('" + month2 + "') then 1 else 0 end) as dt2 ");
            strSql.Append(" FROM (select (select params_name from Param_SysParam where id=CRM_Customer.[CustomerLevel_id]) as CustomerLevel ,* from CRM_Customer) as w1   WHERE isDelete=0 group by CustomerLevel");

            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     客户来源【同比环比】
        /// </summary>
        /// <param name="dt1"></param>
        /// <param name="dt2"></param>
        /// <returns></returns>
        public DataSet Compared_source(string year1, string month1, string year2, string month2)
        {
            var strSql = new StringBuilder();
            strSql.Append(" select CustomerSource as yy,count(CustomerSource) as xx,");
            strSql.Append(" SUM(case when YEAR( Create_date)=('" + year1 + "') and MONTH(Create_date)=('" + month1 + "') then 1 else 0 end) as dt1, ");
            strSql.Append(" SUM(case when YEAR( Create_date)=('" + year2 + "') and MONTH(Create_date)=('" + month2 + "') then 1 else 0 end) as dt2 ");
            strSql.Append(" FROM (select (select params_name from Param_SysParam where id=CRM_Customer.[CustomerSource_id]) as[CustomerSource],* from CRM_Customer) as w1 WHERE isDelete=0  group by CustomerSource");


            return DbHelperSQL.Query(strSql.ToString());
        }

        public DataSet Compared_empcusadd(string year1, string month1, string year2, string month2, string idlist)
        {
            var strSql = new StringBuilder();
            strSql.Append(" select hr_employee.name as yy,");
            strSql.Append(" SUM(case when YEAR( CRM_Customer.Create_date)=('" + year1 +"') and MONTH(CRM_Customer.create_date)=('" + month1 + "') then 1 else 0 end) as dt1, ");
            strSql.Append(" SUM(case when YEAR( CRM_Customer.Create_date)=('" + year2 +"') and MONTH(CRM_Customer.create_date)=('" + month2 + "') then 1 else 0 end) as dt2 ");
            strSql.Append(" FROM hr_employee left outer join  CRM_Customer ");
            strSql.Append(" on hr_employee.ID=CRM_Customer.Create_id ");
            strSql.Append(" where  CRM_Customer.isDelete=0 and hr_employee.ID in " + idlist);
            strSql.Append(" group by hr_employee.name,hr_employee.ID ");
            strSql.Append(" order by hr_employee.ID ");

            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     客户新增统计
        /// </summary>
        public DataSet report_empcus(int year, string idlist)
        {
            var strSql = new StringBuilder();
            strSql.Append(" select name,yy,isnull([1],0) as 'm1',isnull([2],0) as 'm2',isnull([3],0) as 'm3',isnull([4],0) as 'm4',isnull([5],0) as 'm5',isnull([6],0) as 'm6',");
            strSql.Append(" isnull([7],0) as 'm7',isnull([8],0) as 'm8',isnull([9],0) as 'm9',isnull([10],0) as 'm10',isnull([11],0) as 'm11',isnull([12],0) as 'm12' ");
            strSql.Append(" from");
            strSql.Append(" (SELECT   hr_employee.ID, hr_employee.name, COUNT(derivedtbl_1.id) AS cn, YEAR(derivedtbl_1.Create_date) AS yy, ");
            strSql.Append(" MONTH(derivedtbl_1.Create_date) AS mm");
            strSql.Append(" FROM      hr_employee LEFT OUTER JOIN");
            strSql.Append("  (SELECT   id, Create_id, Create_date");
            strSql.Append("  FROM      CRM_Customer");
            strSql.Append("  WHERE isdelete=0 and  (YEAR(Create_date) = " + year +")) AS derivedtbl_1 ON hr_employee.ID = derivedtbl_1.Create_id");
            strSql.Append(" WHERE hr_employee.ID in " + idlist);
            strSql.Append(" GROUP BY hr_employee.ID, hr_employee.name, YEAR(derivedtbl_1.Create_date), MONTH(derivedtbl_1.Create_date)) as tt");
            strSql.Append(" pivot");
            strSql.Append(" (sum(cn) for mm in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12]))");
            strSql.Append(" as pvt");

            return DbHelperSQL.Query(strSql.ToString());
        }


        /// <summary>
        ///     统计漏斗
        /// </summary>
        public DataSet Funnel(string strWhere, string year)
        {
            var strSql = new StringBuilder();
            strSql.Append(" select * from ");
            strSql.Append("( ");
            strSql.Append("	select  ");
            strSql.Append("		a.params_name as CustomerType, ");
            strSql.Append("		a.id as CustomerType_id, ");
            strSql.Append("		a.params_order , ");
            strSql.Append("		COUNT(b.id) as cc  ");
            strSql.Append("	from  ");
            strSql.Append("		Param_SysParam as a left join ( ");
            strSql.Append("			select * from CRM_Customer  ");

            if (year.Trim() != "")
            {
                strSql.Append("			where datediff(year,Create_date,'" + year + "-01-01')=0  ");
            }

            strSql.Append("			)as b  ");
            strSql.Append("		on a.id = b.CustomerType_id  ");
            strSql.Append("	where a.parentid = 1 ");

            if (strWhere.Trim() != "")
            {
                strSql.Append(" and  " + strWhere);
            }

            strSql.Append("	group by  ");
            strSql.Append("		a.params_name, ");
            strSql.Append("		a.id, ");
            strSql.Append("		a.params_order ");
            strSql.Append(") as t1 ");
            strSql.Append("order by params_order ");

            return DbHelperSQL.Query(strSql.ToString());
        }

        #endregion  ExtensionMethod
    }
}