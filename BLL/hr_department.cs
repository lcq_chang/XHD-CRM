﻿/*
* hr_department.cs
*
* 功 能： N/A
* 类 名： hr_department
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-24 10:09:14    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;
using XHD.Common;

namespace XHD.BLL
{
    /// <summary>
    ///     hr_department
    /// </summary>
    public class hr_department
    {
        private readonly DAL.hr_department dal = new DAL.hr_department();

        #region  BasicMethod

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public int Add(Model.hr_department model)
        {
            return dal.Add(model);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.hr_department model)
        {
            return dal.Update(model);
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(int id)
        {
            return dal.Delete(id);
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            return dal.DeleteList(PageValidate.SafeLongFilter(idlist, 0));
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        ///     分页获取数据列表
        ///     分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder, out Total);
        }

        #endregion  BasicMethod

        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}